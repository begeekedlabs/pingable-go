package pingable

import (
	"fmt"
	"net/http"
	"testing"
)

var testData struct {
}

func TestPingableGET(t *testing.T) {
	http.HandleFunc("/ping-test-get", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, `{"response":"pong"}`)
	})
	go http.ListenAndServe(":8080", nil)

	actualResult := Pingable("http://localhost:8080/ping-test-get", `{"action":"ping"}`, `{}`)
	if actualResult.Status != 200 {
		t.Error("Server response incorrect, got", actualResult.Status, "expecting 200")
	}
}

func TestPingablePOST(t *testing.T) {
	http.HandleFunc("/ping-test-post", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, `{"response":"pong"}`)
	})
	go http.ListenAndServe(":8080", nil)

	actualResult := Pingable("http://localhost:8080/ping-test-post", `{"action":"ping"}`, `{"method":"post"}`)
	if actualResult.Status != 200 {
		t.Error("Server response incorrect, got", actualResult.Status, "expecting 200")
	}
}

func TestPingablePUT(t *testing.T) {
	http.HandleFunc("/ping-test-put", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, `{"response":"pong"}`)
	})
	go http.ListenAndServe(":8080", nil)

	actualResult := Pingable("http://localhost:8080/ping-test-put", `{"action":"ping"}`, `{"method":"put"}`)
	if actualResult.Status != 200 {
		t.Error("Server response incorrect, got", actualResult.Status, "expecting 200")
	}
}

func TestPingableDELETE(t *testing.T) {
	http.HandleFunc("/ping-test-delete", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, `{"response":"pong"}`)
	})
	go http.ListenAndServe(":8080", nil)

	actualResult := Pingable("http://localhost:8080/ping-test-delete", `{"action":"ping"}`, `{"method":"delete"}`)
	if actualResult.Status != 200 {
		t.Error("Server response incorrect, got", actualResult.Status, "expecting 200")
	}
}
