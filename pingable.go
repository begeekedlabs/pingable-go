package pingable

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type pingableOptions struct {
	Key       string `json:"key"`        // Default ""
	Method    string `json:"method"`     // default "post"
	Timeout   int    `json:"timeout"`    // default 10 seconds
	KeyHeader string `json:"key-header"` // default "x-api-key"
	Version   string `json:"version"`    // default "1.0"
}

type pingableResponse struct {
	Status int
	Error  string
	Data   string
}

func Pingable(address string, Payload string, options string) pingableResponse {

	// Set the defaults
	pingableOptions := pingableOptions{
		Key:       "",
		Method:    "get",
		Timeout:   10,
		KeyHeader: "x-api-key",
		Version:   "1.0",
	}
	err := json.Unmarshal([]byte(options), &pingableOptions)
	if err != nil {
		return pingableResponse{
			Status: 0,
			Error:  "pingable go error: " + err.Error(),
			Data:   "{}",
		}
	}

	requestTimeout := time.Duration(pingableOptions.Timeout) * time.Second

	httpClient := &http.Client{
		Timeout: requestTimeout,
	}

	var request *http.Request
	var dataReader io.Reader

	if pingableOptions.Method == "get" || pingableOptions.Method == "GET" {
		request, err = http.NewRequest("GET", address, nil)
	} else if pingableOptions.Method == "post" || pingableOptions.Method == "POST" {
		dataReader = strings.NewReader(Payload)
		request, err = http.NewRequest("POST", address, dataReader)
	} else if pingableOptions.Method == "put" || pingableOptions.Method == "PUT" {
		dataReader = strings.NewReader(Payload)
		request, err = http.NewRequest("PUT", address, dataReader)
	} else if pingableOptions.Method == "delete" || pingableOptions.Method == "DELETE" {
		request, err = http.NewRequest("DELETE", address, nil)
	}
	if err != nil {
		return pingableResponse{
			Status: 0,
			Error:  "pingable go error: " + err.Error(),
			Data:   "{}",
		}
	}
	request.Header.Set("User-Agent", "Pingable 1.0")
	if pingableOptions.Key != "" {
		request.Header.Set(pingableOptions.KeyHeader, pingableOptions.Key)
	}

	response, err := httpClient.Do(request)
	if err != nil {
		return pingableResponse{
			Status: 0,
			Error:  "pingable go error: " + err.Error(),
		}
	}
	defer response.Body.Close()

	responseData, _ := ioutil.ReadAll(response.Body)

	return pingableResponse{
		Status: response.StatusCode,
		Error:  "",
		Data:   string(responseData),
	}

}
